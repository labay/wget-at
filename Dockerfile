FROM python:3.8-alpine
ENV LC_ALL=C.UTF-8
RUN apk update \
 && apk add --no-cache git wget \
    build-base pkgconfig coreutils autoconf autoconf-archive automake texinfo \
    perl flex gperf \
    gettext-dev gpgme-dev zstd-dev gnutls-dev c-ares-dev lua5.1-dev luajit-dev
RUN git clone https://github.com/ArchiveTeam/wget-lua \
 && cd wget-lua \
 && ./bootstrap \
 && ./configure --with-ssl=gnutls --with-cares -disable-nls \
 && make \
 && make install
RUN cd / \
 && rm -rf wget-lua \
 && apk del build-base pkgconfig coreutils autoconf autoconf-archive automake texinfo
